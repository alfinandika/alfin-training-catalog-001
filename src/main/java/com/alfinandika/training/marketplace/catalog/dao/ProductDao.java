package com.alfinandika.training.marketplace.catalog.dao;

import com.alfinandika.training.marketplace.catalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {

}
